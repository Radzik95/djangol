from django.db import models
from django.contrib.auth.models import AbstractUser


class Role(models.Model):
    class RoleType(models.TextChoices):
        TEACHER = (
            "T",
            "Teacher",
        )  # Tuaj wpisz jakie role, z lewej zamiast T też sobie wpisz teacher i porównuj w views.py linijka 20  == 'Teacher' zamiast "T"
        STUDENT = "S", "Student"

    type = models.CharField(
        max_length=2,
        choices=RoleType.choices,
        default=RoleType.STUDENT,
    )

    def __str__(self):
        return self.type


class User(AbstractUser):
    # Tutaj bedziemy przypisywac jakie role ma ten użytkownik to nawet nie koniecznie FK musiałoby być tylko zwykły wybór
    role = models.ForeignKey(
        Role,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="users",
        verbose_name="role",
    )


"""
    Jakbyś chciał model studenta to tak samo klepnąć OneToOne Student model do Usera i tam wpisz jakie pola ma mieć student

"""


class Test(models.Model):
    test = models.PositiveIntegerField(blank=True, null=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)