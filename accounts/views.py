from django.shortcuts import render
from django.views.generic import ListView
from .models import Test


from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test


def student_required(
    function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url="login"
):
    """
    Decorator for views that checks that the logged in user is a student,
    redirects to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(
        lambda user: user.is_active
        # tutaj sprawdza czy jest studentem jeśli jest i zalogowany to pozwoli mu wejsc na ten widok
        and user.role.type == "S",
        login_url=login_url,
        redirect_field_name=redirect_field_name,
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


@method_decorator([login_required, student_required], name="dispatch")
class StudentOnlyView(ListView):
    template_name = "accounts/test.html"
    queryset = Test.objects.all()

    def get_queryset(self):
        # wyświetla tylko te testy które stworzył dany użytkownik
        # można zamienić created_by na assigment czyli przypisane i tylko
        # wyświetlaj przypisane do aktualnie zalogowanego usera
        return Test.objects.filter(created_by=self.request.user)