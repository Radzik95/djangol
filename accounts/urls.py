from django.urls import path
from .views import StudentOnlyView


urlpatterns = [
    path("for-students", StudentOnlyView.as_view()),
]