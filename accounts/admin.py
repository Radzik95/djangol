from django.contrib import admin
from django.contrib.auth.models import Group


from django.contrib.auth.admin import UserAdmin as BaseUserAdmin


from .models import User, Role, Test


class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            "Personal info",
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "role",
                )
            },
        ),
        ("Important dates", {"fields": ("last_login", "date_joined")}),
    )


class RoleAdmin(admin.ModelAdmin):
    pass


class TestAdmin(admin.ModelAdmin):
    pass


admin.site.register(Test, TestAdmin)
admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
admin.site.register(Role, RoleAdmin)